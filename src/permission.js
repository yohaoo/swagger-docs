import router from './router'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { getToken, getModule } from '@/utils/common/auth' // getToken from cookie
import main from '@/utils/main'
import store from './store'
// NProgress.configure({ showSpinner: false })// NProgress Configuration

// permission judge function
// function hasPermission(roles, permissionRoles) {
//   if (roles.indexOf('admin') >= 0) return true // admin permission passed directly
//   if (!permissionRoles) return true
//   return roles.some(role => permissionRoles.indexOf(role) >= 0)
// }

router.beforeEach((to, from, next) => {
    NProgress.start() // start progress bar
        // debugger
    if (store.getters.treePaths.length === 0) {
        store.dispatch('GenerateRoutes', {}).then(() => {
            next({...to, replace: true })
        })
    } else {
        next()
        NProgress.done()
    }
})
router.afterEach(() => {
    NProgress.done() // finish progress bar
})