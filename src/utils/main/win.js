import { Notification } from 'element-ui';

export default {
  /**
   * 全局消息提示
   * @param {type} type 类型：success error warning
   * @param {string} message 消息内容
   */
  message (type,title, message) {
    Notification({
      title: title||'系统提示',
      message: message||'提示内容',
      type: type
    })
  },
    /**
   * 关闭当前路由
   * @param {type} type 类型：success error warning
   * @param {string} message 消息内容
   */
  closeTag (that) {
    let view= that.$route;
    // console.log(that.$store.state.tagsView.visitedViews)
    that.$store.dispatch('delView', view).then(({ visitedViews }) => {
        const latestView = visitedViews.slice(-1)[0]
        if (latestView) {
          that.$router.push(latestView)
        } else {
          that.$router.push('/')
        }
    })
  },
};
