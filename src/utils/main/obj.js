export default {
    /**
     * element 表单价格验证
     * @param {*} rule
     * @param {*} value 值
     * @param {*} callback 回调
     */
    VerifyInputPrice(rule, value, callback) {
        if (!value && value !== 0) {
            return callback(new Error('值不能为空'));
        }
        if (value > 1000000000) {
            return callback(new Error('值不能大于10,0000,0000'));
        }
        if (value < 0) {
            return callback(new Error('值不能小于0'));
        }
        if (parseFloat(value).toFixed(2) < parseFloat(value)) {
            return callback(new Error('请保留两位小数'));
        }
        return callback();
    },
    /**
     * element 表单数量验证
     * @param {*} rule
     * @param {*} value 值
     * @param {*} callback 回调
     */
    VerifyInputQyt(rule, value, callback) {
        if (!value && value !== 0) {
            return callback(new Error('值不能为空'));
        }
        if (value > 1000000000) {
            return callback(new Error('值不能大于10,0000,0000'));
        }
        if (value < 0) {
            return callback(new Error('值不能小于0'));
        }
        var r = /^\+?[1-9][0-9]*$/;
        if (!r.test(value.toString())) {
            return callback(new Error('请输入正整数'));
        }
        return callback();
    },
    /**
     * element 表单数量验证数量不能为0或者小于0
     * @param {*} rule
     * @param {*} value 值
     * @param {*} callback 回调
     */
    VerifyInputQytOne(rule, value, callback) {
        if (!value && value !== 0) {
            return callback(new Error('值不能为空'));
        }
        if (value > 1000000000) {
            return callback(new Error('值不能大于10,0000,0000'));
        }
        if (value <= 0) {
            return callback(new Error('值不能小于0'));
        }
        var r = /^\+?[1-9][0-9]*$/;
        if (!r.test(value.toString())) {
            return callback(new Error('请输入正整数'));
        }
        return callback();
    },
    /**
     * element 表单验证限制只能输入（数字和字母）
     * @param {*} rule
     * @param {*} value 值
     * @param {*} callback 回调
     */
    VerifyInputAlphOrNum(rule, value, callback) {
        if (!value) {
            return callback(new Error('值不能为空'));
        }
        var r = /^[0-9a-zA-Z]*$/g
        if (!r.test(value.toString())) {
            return callback(new Error('只能输入数字和字母'));
        }
        return callback();
    },
    /**
     * element 表单手机简单验证限制只能输入11位数字
     * @param {*} rule
     * @param {*} value 值
     * @param {*} callback 回调
     */
    VerifyInputPhone(rule, value, callback) {
        var res = /^1\d{10}$/
        if (!res.test(value)) {
            return callback(new Error('输入的手机号格式有误'))
        }
        return callback()
    },
    /**
     * element 过滤后台数据表单需要的字段
     * @param {*} form 表单
     * @param {*} data 后台数据
     */
    FiltrationData(form, data) {
        for (const key in form) {
            if (form.hasOwnProperty(key)) {
                form[key] = data[key]
            }
        }
    },
    /**
     *
     * @param {*} fn 执行方法
     * @param {*} time 时间间隔 （毫秒）
     */
    _debounce(fn, time) {
        let timeout = null;
        return function() {
            clearTimeout(timeout);
            timeout = setTimeout(() => {
                fn.apply(this, arguments)
            }, time || 500);
        }
    },
    SaveRefreshtime() {
        let nowtime = new Date();
        let lastRefreshtime = window.localStorage.refreshtime ? new Date(window.localStorage.refreshtime) : new Date(-1);
        let expiretime = new Date(Date.parse(window.localStorage.TokenExpire))

        let refreshCount = 20; //滑动系数

        if (lastRefreshtime >= nowtime) {
            lastRefreshtime = nowtime > expiretime ? nowtime : expiretime;
            lastRefreshtime.setMinutes(lastRefreshtime.getMinutes() + refreshCount);
            window.localStorage.refreshtime = lastRefreshtime;
        } else {
            window.localStorage.refreshtime = new Date(-1);
        }
    },
    /**
     * 获取对象的第一个属性
     * @param {*} data 
     */
    get_object_first_attribute(data) {
        for (var key in data) return key;
    }
}