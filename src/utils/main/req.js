// 引入axios
import axios from 'axios'

axios.interceptors.request.use(config => {
    return config
}, error => {
    Promise.reject(error)
})

// 响应拦截器即异常处理
axios.interceptors.response.use(response => {
    return response
}, err => {
    // console.log(err)
    return ""; // 返回接口返回的错误信息
})

axios.defaults.timeout = 50000

export default {
    // get请求
    get(url, param) {
        return axios.get(url, {
            params: param
        }).then((res) => {
            return Promise.resolve(res.data)
        })
    },
    /**
     * @param {string} url 请求地址
     * @param {object} param  请求参数
     */
    post(url, param, ) {
        return axios.post(url, param).then((res) => {
            return Promise.resolve(res.data)
        }).catch(msg => {
            return Promise.reject()
        })
    },

}