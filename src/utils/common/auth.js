import Cookies from 'js-cookie'
import { getLocal, setLocal, removeLocal } from './cache'

const TokenKey = 'jqyxglpt_admin_token'
const MenuKey = 'swagger_docs_data'
const UserKey = 'jqyxglpt_admin_User'

// token
export function getToken() {
    return Cookies.get(TokenKey)
}

export function setToken(token) {
    return Cookies.set(TokenKey, token)
}

export function removeToken() {
    return Cookies.remove(TokenKey)
}
// 菜单
export function getModule() {
    return getLocal(MenuKey)
}

export function setModule(modules) {
    return setLocal(MenuKey, modules)
}

export function removeModule() {
    return removeLocal(MenuKey)
}
// 用户信息
export function getUser() {
    return getLocal(UserKey)
}

export function setUser(user) {
    return setLocal(UserKey, user)
}

export function removeUser() {
    return removeLocal(UserKey)
}