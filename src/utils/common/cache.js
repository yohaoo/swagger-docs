// 获取cookie、
export function getCookie (name) {
  var arr, reg = new RegExp('(^| )' + name + '=([^;]*)(;|$)')
  if (arr = document.cookie.match(reg)) {
    return (arr[2])
  } else {
    return null
  }
}
// 设置cookie,增加到vue实例方便全局调用
export function setCookie (cname, value, expiredays) {
  var exdate = new Date()
  exdate.setDate(exdate.getDate() + expiredays)
  document.cookie = cname + '=' + escape(value) + ((expiredays == null) ? '' : ';expires=' + exdate.toGMTString())
};

// 删除cookie
export function delCookie (name) {
  var exp = new Date()
  exp.setTime(exp.getTime() - 1)
  var cval = getCookie(name)
  if (cval != null) {
    document.cookie = name + '=' + cval + ';expires=' + exp.toGMTString()
  }
};

// 获取Local
export function getLocal (name) {
  if (!name) return
  return window.localStorage.getItem(name)
};
// 设置local
export function setLocal (name, content) {
  if (!name) return
  if (typeof content !== 'string') {
      content = JSON.stringify(content)
  }
  window.localStorage.setItem(name, content)
};

// 删除Local
export function removeLocal (name) {
  if (!name) return
  window.localStorage.removeItem(name)
};

