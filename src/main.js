// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store';
import ElementUI from 'element-ui';
import Print from 'vue-print-nb' //打印
import 'element-ui/lib/theme-chalk/index.css';

//全局公用方法
import main from "./utils/main";

//全局路由拦截 防止刷新界面 仓库清空
import './permission'; // permission control

import JSONView from 'vue-json-viewer'

import jsoneditor from 'jsoneditor'
// 清除样式
import './css/reset.css'
import './css/global.css'
import '@/styles/index.scss' // global css

import axios from 'axios'

Vue.use(ElementUI);
Vue.use(Print)
Vue.use(JSONView)

Vue.config.productionTip = false
Vue.prototype.$main = main;
Vue.prototype.$jsoneditor = jsoneditor

//不是调试环境的时候
if (process.env.NODE_ENV !== 'development') {

    axios.get('./serverconfig.json').then(res => {
        console.log('xxx',res)
        if (res.data.isEnableDeployUrl)
            Vue.prototype.baseUrl = res.data.deployUrl;
        else {
            Vue.prototype.baseUrl = document.location.protocol + "//" + location.host;
        }
        Vue.prototype.config = res.data;
        init();
    });
} else {
    Vue.prototype.config = {
        "defaultVersion": "Sys",
        "developmentUrl": "http://localhost:61301"
    };
    Vue.prototype.baseUrl = 'http://localhost:61301'
    init();
}

// axios.get('./serverconfig.json').then(res => {
//     if (!res.data.isEnableDeployUrl)
//         Vue.prototype.baseUrl = res.data.deployUrl;
//     else {
//         Vue.prototype.baseUrl = document.location.protocol + "//" + location.host;
//     }
//     if (process.env.NODE_ENV === 'development') {
//         Vue.prototype.baseUrl = res.data.developmentUrl;
//     }
//     Vue.prototype.config = res.data;
//     init();
// });


function init() {
    new Vue({
        el: '#app',
        router,
        store,
        render: h => h(App),
        created() {

        }
    })
}
/* eslint-disable no-new */