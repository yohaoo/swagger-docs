import Vue from 'vue'
import Vuex from 'vuex'
import tagsView from './modules/tagsView'
import user from './modules/user'
import getters from './getters'
import permission from './modules/permission'
import app from './modules/app'
Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    app,
    tagsView,
    user,
    permission,
  },
  getters
})

export default store
