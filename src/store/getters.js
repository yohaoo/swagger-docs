const getters = {
    visitedViews: state => state.tagsView.visitedViews,
    cachedViews: state => state.tagsView.cachedViews,
    token: state => state.user.token,
    avatar: state => state.user.avatar,
    name: state => state.user.name,
    introduction: state => state.user.introduction,
    status: state => state.user.status,
    roles: state => state.user.roles,
    setting: state => state.user.setting,

    paths: state => state.permission.paths,
    treePaths: state => state.permission.treePaths,
    models: state => state.permission.models,
    host: state => state.permission.host,
    info: state => state.permission.info,
    muiltVersion: state => state.permission.muiltVersion,

    sidebar: state => state.app.sidebar,
}
export default getters