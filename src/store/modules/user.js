// import { loginByUsername, logout, getUserInfo } from '@/api/login'
import { getToken, setToken, removeToken, removeModule, setUser, getUser } from '@/utils/common/auth'
import req from '@/utils/main/req'

const user = {
    state: {
        user: '',
        status: '',
        code: '',
        token: getToken(),
        name: '',
        avatar: '',
        introduction: '',
        roles: [],
        setting: {
            articlePlatform: []
        },
        expiredate: '', //过期时间
    },

    mutations: {
        SET_CODE: (state, code) => {
            state.code = code
        },
        SET_TOKEN: (state, token) => {
            state.token = token
        },
        SET_INTRODUCTION: (state, introduction) => {
            state.introduction = introduction
        },
        SET_SETTING: (state, setting) => {
            state.setting = setting
        },
        SET_STATUS: (state, status) => {
            state.status = status
        },
        SET_NAME: (state, name) => {
            state.name = name
        },
        SET_AVATAR: (state, avatar) => {
            state.avatar = avatar
        },
        SET_ROLES: (state, roles) => {
            state.roles = roles
        },
        SET_TokenExpire: (state, expiredate) => {
            state.expiredate = expiredate
        }
    },

    actions: {
        // 用户名登录
        LoginByUsername({ commit }, userInfo) {
            const phone = userInfo.username.trim()
            return new Promise((resolve, reject) => {
                req.post('Sys/SysUser/Login', { account: phone, pwd: userInfo.password }).then(res => {

                    commit('SET_TOKEN', res.data.token)
                        // commit('SET_AVATAR', res.Data.Avatar)
                    commit('SET_NAME', res.data.usreName)
                        //------报存过期时间 Start
                    var curTime = new Date();
                    var expiredate = new Date(curTime.setSeconds(curTime.getSeconds() + res.data.expires_in));
                    commit("SET_TokenExpire", expiredate); // 保存过期时间
                    window.localStorage.refreshtime = expiredate;
                    window.localStorage.TokenExpire = expiredate;
                    //----end

                    setToken(res.data.token)
                    setUser(res.data)
                    resolve()
                }).catch(res => {
                    reject(res);
                })
            })
        },
        SetToken({ commit }, token) {
            commit('SET_TOKEN', token)
            setToken(token)
        },

        // 获取用户信息
        GetUserInfo({ commit, state }) {
            return new Promise((resolve, reject) => {
                let userinfo = JSON.parse(getUser())
                commit('SET_NAME', userinfo.UserName)
                resolve()
            })
        },

        // 第三方验证登录
        // LoginByThirdparty({ commit, state }, code) {
        //   return new Promise((resolve, reject) => {
        //     commit('SET_CODE', code)
        //     loginByThirdparty(state.status, state.email, state.code).then(response => {
        //       commit('SET_TOKEN', response.data.token)
        //       setToken(response.data.token)
        //       resolve()
        //     }).catch(error => {
        //       reject(error)
        //     })
        //   })
        // },

        // 登出
        LogOut({ commit, state }) {
            return new Promise((resolve, reject) => {
                commit('SET_TOKEN', '')
                commit('SET_ROLES', [])
                commit('SET_TokenExpire', '')
                commit('Delete_SET_ROUTERS', '')
                commit('DEL_ALL_VISITED_VIEWS', '')


                removeToken()
                removeModule()
                window.localStorage.refreshtime = '';
                window.localStorage.TokenExpire = '';
                resolve()
            })
        },

        // 前端 登出
        // FedLogOut({ commit }) {
        //   return new Promise(resolve => {
        //     commit('SET_TOKEN', '')
        //     removeToken()
        //     resolve()
        //   })
        // },

        // 动态修改权限
        ChangeRoles({ commit, dispatch }, role) {
            return new Promise(resolve => {
                commit('SET_TOKEN', role)
                setToken(role)
                getUserInfo(role).then(response => {
                    const data = response.data
                    commit('SET_ROLES', data.roles)
                    commit('SET_NAME', data.name)
                    commit('SET_AVATAR', data.avatar)
                    commit('SET_INTRODUCTION', data.introduction)
                    dispatch('GenerateRoutes', data) // 动态修改权限后 重绘侧边菜单
                    resolve()
                })
            })
        }
    }
}

export default user