import { getModule, setModule } from '@/utils/common/auth'
import req from '@/utils/main/req'
import obj from '@/utils/main/obj'
import config from '../../../static/config'
import Vue from 'vue'
import axios from 'axios'
//索引
let pIndex = 1;
/**
 * 格式化 返回接口路径
 * @param {* paths路径} paths
 */
function formatPath(paths) {
    var arrayPaths = [];
    for (const key in paths) {
        let method = obj.get_object_first_attribute(paths[key]);
        if (paths.hasOwnProperty(key)) {
            arrayPaths.push({
                path: key,
                method: method,
                data: paths[key][method],
                name: paths[key][method].tags[0],
                description: paths[key][method].summary,
                id: pIndex++,
                children: []
            });
        }
    }
    return arrayPaths;
}

/**
 *
 * @param {* 树形的接口路径} treePaths
 * @param {* 接口的类别集合 } tags
 * @param {* 格式化后的 paths} paths
 */
function setTreePath(treePaths, tags, paths) {
    tags.forEach((el) => {
        el['children'] = paths.filter(v => { return v.name == el.name; });
        el['id'] = pIndex++;
    });
    treePaths.push(...tags);
}

function setModel(models, data) {
    for (const key in data) {
        if (data.hasOwnProperty(key)) {
            models.push({
                name: key,
                properties: data[key].properties,
                type: data[key].type,
                description: data[key].description,
            });
        }
    }
}
const permission = {
    state: {
        paths: [], //格式化后的地址
        treePaths: [], //树形paths
        models: [], //实体类
        host: '',
        info: {},
        schemes: '',
        muiltVersion: {}, //多个版本
        defaultVersion: '', //默认版本
    },
    mutations: {
        SET_PATHS: (state, alldata) => {
            state.paths = formatPath(alldata.paths);

            setTreePath(state.treePaths, alldata.tags, state.paths);

            setModel(state.models, alldata.definitions);

            state.host = alldata.host;

            state.info = alldata.info;

            state.schemes = alldata.schemes['0'];

            state.muiltVersion = alldata.muiltVersion;
        },
        SET_DEFAULTVERSION: (state, defaultV) => {
            state.defaultVersion = defaultV;
        },
        CLEARALL: (state) => {
            state.paths = [];
            state.treePaths = []
            state.models = [];
            state.host = '';
            state.info = {};
            state.schemes = '';
            state.muiltVersion = {};
        }
    },
    actions: {
        GenerateRoutes({
            commit
        }, data) {
            return new Promise(resolve => {
                if (getModule() && config.defaultVersion == permission.state.defaultVersion) {
                    var all_data = JSON.parse(getModule())
                    commit('SET_DEFAULTVERSION', config.defaultVersion)
                    commit('SET_PATHS', all_data)
                    resolve()
                } else {
                    // console.log("我就想看看config", Vue.prototype.config)

                    commit('SET_DEFAULTVERSION', Vue.prototype.config.defaultVersion)
                    axios({
                        method: "post",
                        url: Vue.prototype.baseUrl + '/swagger/docs/' + Vue.prototype.config.defaultVersion,
                        data: {}
                    }).then(res => {
                        setModule(res)
                        commit('SET_PATHS', res.data)
                        resolve()
                    });

                    // req.post('/swagger/docs/' + Vue.prototype.config.defaultVersion, {}).then(res => {
                    //     setModule(res)
                    //     commit('SET_PATHS', res)
                    //     resolve()
                    // })
                }
            })
        },
        ChangeVersion({ commit }, data) {
            return new Promise(resolve => {
                commit('SET_DEFAULTVERSION', data)
                req.post( Vue.prototype.baseUrl +'/swagger/docs/' + data, {}).then(res => {
                    setModule(res)
                    commit('CLEARALL')
                    commit('SET_PATHS', res)
                    resolve()
                })
            })
        }
    }
}

export default permission