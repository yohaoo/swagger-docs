/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/index'

const componentsRouter = {
  path: '/components',
  component: Layout,
  redirect: 'noredirect',
  name: 'ComponentDemo',
  meta: {
    title: 'components',
    icon: 'component'
  },
  children: [
    // {
    //   path: 'tinymce',
    //   component: () => import('@/views/components-demo/tinymce'),
    //   name: 'TinymceDemo',
    //   meta: { title: '富文本编辑器' }
    // },
    // {
    //   path: 'markdown',
    //   component: () => import('@/views/components-demo/markdown'),
    //   name: 'MarkdownDemo',
    //   meta: { title: 'markdown' }
    // }
  ]
}

export default componentsRouter
