/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/views/layout/index'

const ortherRouter = {
  path: '/orthers',
  component: Layout,
  redirect: 'noredirect',
  name: 'orthers',
  meta: {
    title: 'orther',
    icon: 'orther'
  },
  children: [
    // {
    //   path: 'articleAdd/:id',
    //   component: () => import('@/views/yohao/blog/article/add'),
    //   name: 'articleAdd',
    //   meta: { title: '操作博客文章' }
    // }
  ]
}

export default ortherRouter
