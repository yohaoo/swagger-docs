import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/views/layout/index'
import componentsRouter from './modules/components'
import orthersRouter from './modules/orthers'
import NotFound from '@/views/redirect/404'

Vue.use(Router)
export const constantRouterMap = [{
            path: '',
            component: Layout,
            hidden: true,
            children: [{
                path: '/info/:id',
                component: () =>
                    import ('@/views/interface/index'),
            }]
        },
        {
            path: '/redirect',
            component: Layout,
            hidden: true,
            children: [{
                path: '/redirect/:path*',
                component: () =>
                    import ('@/views/redirect/index')
            }]
        },
        {
            path: '/login',
            component: () =>
                import ('@/views/login/index'),
            hidden: true
        },
        {
            path: '/nofound',
            component: Layout,
            hidden: true
        },
        // { path: '*', component: NotFound },
        // componentsRouter,
        // orthersRouter,
    ]
    ///Depot/DepotManage
    // {
    //   path: '/',
    //   name: 'HelloWorld',
    //   component: HelloWorld
    // },
    // {
    //   path: '/dialog',
    //   name: 'dialog',
    //   component: Dialog
    // },
    // {
    //   path: '/yohao',
    //   name: 'yohao',
    //   component: yohao
    // }
export default new Router({
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRouterMap
})