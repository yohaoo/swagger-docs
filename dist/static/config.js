export default {
    "description": "[defaultVersion:默认版本]、[isEnableDeployUrl:否启用配置的url，默认关闭，将启用当前域的地址，开启后，将请求指定的接口文档地址]",
    "description2": "[deployUrl:自定义配置地址]、[developmentUrl:开发环境地址]",
    "defaultVersion": "Sys",
    "isEnableDeployUrl": "false",
    "deployUrl": "http://wwww.xxx.com",
    "developmentUrl": "http://localhost:61301"
}