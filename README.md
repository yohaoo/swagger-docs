# yohao swagger-docs

> 这是一个基于swagger文档，已经实现了动态配置，无需更改源码，直接拿dist打包后的文件即可。

### 项目基本操作
如果你不想重新编译打包，就直接把dist里面的文件放在后台的swagger目录里面。
这个项目默认的打包目录assetsPublicPath  是swagger，如果需要将这个单独放在一个站点里面的，需要把这个去掉。

### serverconfig json 文件的配置
```json
{
    "defaultVersion": "Sys",
    "isEnableDeployUrl": "false",
    "deployUrl": "http://wwww.xxx.com",
    "developmentUrl": "http://localhost:61301"
}
```
defaultVersion 这个是默认的区域，这个很重要，第一次配置需要将这个改好。
isEnableDeployUrl 这个是否启用自定义的接口地址，如果为true 请求的接口文档地址就会拿 deployUrl设置的默认地址，
developmentUrl  这个是本地开发的时候用的后台文档地址

### 结束语
如果各位大佬有更好的提议,热烈欢迎各位Issue。 



